import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import createSagaMiddleware from 'redux-saga';

import { App } from './components/App';
import { countReducer } from './reducers/counter';
import counterSaga from './sagas/counterSaga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(countReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(counterSaga);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
