import { put, takeEvery, all } from 'redux-saga/effects';
import * as types from '../constants';

const delay = (ms: number) => new Promise(res => setTimeout(res, ms));

function* helloSaga() {
  console.log('---Hello Sagas!---');
}

function* incrementAsync() {
  yield delay(1000);
  yield put({ type: types.INCREMENT });
}

function* watchIncrementAsync() {
  yield takeEvery('INCREMENT_ASYNC', incrementAsync);
}

function* decrementAsync() {
  yield delay(1000);
  yield put({ type: types.DECREMENT });
}

function* watchDecrementAsync() {
  yield takeEvery('DECREMENT_ASYNC', decrementAsync);
}

function* resetAsync() {
  yield delay(1000);
  yield put({ type: types.RESET });
}

function* watchResetAsync() {
  yield takeEvery('RESET_ASYNC', resetAsync);
}

export default function* rootSaga() {
  yield all([
    helloSaga(),
    watchIncrementAsync(),
    watchDecrementAsync(),
    watchResetAsync(),
  ]);
}
