import * as React from 'react';
import { FC } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Counter from '../containers/Counter';
import './Home.scss';

const styles: React.CSSProperties = {
  display: 'flex',
  flexDirection: 'row',
};

export const Home: FC<RouteComponentProps> = (
  props: RouteComponentProps
): JSX.Element => {
  console.log(props);

  const i: React.ReactNode = <div />;
  console.log(i);

  return (
    <div className="border">
      Home page
      <ul style={styles}>
        <li>1</li>
        <li>2</li>
      </ul>
      <Counter />
      <img src={require('../img/halflife.jpg').default} alt="halflife" />
    </div>
  );
};
