import * as React from 'react';
import { FunctionComponent } from 'react';

export const Hello: FunctionComponent = () => {
  const [showMessage, setShowMessage] = React.useState(false);
  React.useEffect(() => {
    if (showMessage) {
      document.title = 'ON';
    } else {
      document.title = 'OFF';
    }
  }, [showMessage]);

  return (
    <div>
      <h1>Hello!</h1>
      <button
        onClick={(): void => setShowMessage((prev: boolean): boolean => !prev)}
      >
        toggler
      </button>
      {showMessage ? (
        <div className="green">SHOW</div>
      ) : (
        <div className="error">HIDE</div>
      )}
    </div>
  );
};
