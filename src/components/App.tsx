import * as React from 'react';
import { FunctionComponent } from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';
import { About } from './About';
import { Home } from './Home';
import { Hello } from './Hello';
import '../styles/main.scss';

export const App: FunctionComponent = () => {
  return (
    <div>
      <h1>Hello world</h1>
      <ul>
        <li>
          <NavLink exact to="/">
            Home {'\u00A9 ' + String.fromCharCode(169)}
          </NavLink>
        </li>
        <li>
          <NavLink to="/about">About</NavLink>
        </li>
      </ul>
      <Switch>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/" component={Home} />
      </Switch>
      <Hello />
    </div>
  );
};
