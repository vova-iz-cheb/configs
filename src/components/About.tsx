import * as React from 'react';
import { FunctionComponent } from 'react';

type F = (a: number, b: number) => number;

const sum: F = function(a, b) {
  return a + b;
};

// eslint-disable-next-line prettier/prettier
function d(x:number, y:number):number;
function d(x:string, y:string):string;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function d(x:any,y:any):any {
  return x + y;
}

console.log(d(10,10))
console.log(d('Pri','vet'));

export const About: FunctionComponent = () => {
  return <div>About this site {sum(15, 6)}</div>;
};
