import * as types from '../constants';

export interface IAction {
  type: string;
  payload?: number;
}

export function increment(): IAction {
  return {
    type: types.INCREMENT,
  };
}

export function decrement(): IAction {
  return {
    type: types.DECREMENT,
  };
}

export function reset(): IAction {
  return {
    type: types.RESET,
  };
}
