import * as types from '../constants';
import { IAction } from '../actions/counter';

export interface IInitialState {
  count: number;
}

const initialState: IInitialState = {
  count: 0,
};

export function countReducer(
  state: IInitialState = initialState,
  action: IAction
) {
  switch (action.type) {
    case types.INCREMENT:
      return {
        ...state,
        count: state.count + 1,
      };
    case types.DECREMENT:
      return {
        ...state,
        count: state.count - 1,
      };
    case types.RESET:
      return initialState;
    default:
      return state;
  }
}
