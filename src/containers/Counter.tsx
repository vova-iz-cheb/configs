import * as React from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { IInitialState } from '../reducers/counter';
import * as actions from '../actions/counter';

type Props = {
  count: number,
  dispatch: Dispatch,
};

export const Counter: React.FC<Props> = props => {
  const { count, dispatch } = props;

  const handleClick: React.ReactEventHandler<HTMLButtonElement> = e => {
    console.log(e);
    dispatch(actions.increment());
  };

  return (
    <div>
      <h3>Counter:</h3>
      <p>{count}</p>
      <button onClick={handleClick}>Increment</button>{' '}
      <button onClick={() => dispatch(actions.decrement())}>Decrement</button>{' '}
      <button onClick={() => dispatch(actions.reset())}>Reset</button>{' '}
      <button
        onClick={() =>
          dispatch({
            type: 'INCREMENT_ASYNC',
          })
        }
      >
        ASYNC Inr
      </button>
      <button
        onClick={() =>
          dispatch({
            type: 'DECREMENT_ASYNC',
          })
        }
      >
        ASYNC Dec
      </button>
      <button
        onClick={() =>
          dispatch({
            type: 'RESET_ASYNC',
          })
        }
      >
        ASYNC Reset
      </button>
    </div>
  );
};

const mapStateToProps = (state: IInitialState): IInitialState => {
  return {
    count: state.count,
  };
};

export default connect(mapStateToProps)(Counter);
