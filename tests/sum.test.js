// import { increment } from "../src/actions/counter";

// function sum(a, b) {
//   return a + b;
// }

// test("adds 1 + 2 to equal 3", () => {
//   expect(sum(1, 2)).toBe(3);
// });

// test("adds 1 + 2 not to equal 4", () => {
//   expect(sum(1, 2)).not.toBe(4);
// });

// test("increment", () => {
//   expect(increment()).toEqual({ type: "INCREMENT" });
// });

function forEach(arr, callback) {
  for (let i = 0; i < arr.length; i++) {
    callback(i);
  }
}

beforeAll(() => {
  console.log('begin');
});
afterAll(() => {
  console.log('finish');
});

const mockCallBack = jest.fn(() => 1);

forEach([0, 1, 2], mockCallBack);

describe('one', () => {
  test('mock', () => {
    expect(mockCallBack.mock.calls.length).toBe(3);
  });
});

describe('two', () => {
  test('mock', () => {
    expect(mockCallBack.mock.calls.length).toBe(3);
  });
});
