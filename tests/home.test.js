// import dependencies
import React from 'react';

// import react-testing methods
import { render } from '@testing-library/react';

// add custom jest matchers from jest-dom
import '@testing-library/jest-dom/extend-expect';

import { Home } from '../src/components/Home';

test('dom', () => {
  const obj = render(<Home />);
  obj.debug();
  expect(1).toBe(1);
});
