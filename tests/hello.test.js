// import dependencies
import React from 'react';

// import react-testing methods
import { render, fireEvent } from '@testing-library/react';

// add custom jest matchers from jest-dom
import '@testing-library/jest-dom/extend-expect';

import { Hello } from '../src/components/Hello';

test('dom', () => {
  const obj = render(<Hello />);
  obj.debug();
  expect(obj.getByText('Hello!')).toBeInTheDocument();
  expect(obj.getByText(/hide/i)).toHaveClass('error');
  expect(obj.queryByText(/show/i)).not.toBeInTheDocument();

  fireEvent.click(obj.getByText('toggler'));

  expect(obj.queryByText(/hide/i)).not.toBeInTheDocument();
  expect(obj.getByText(/show/i)).toHaveClass('green');

  fireEvent.click(obj.getByText('toggler'));

  expect(obj.getByText(/hide/i)).toHaveClass('error');
  expect(obj.queryByText(/show/i)).not.toBeInTheDocument();
});
