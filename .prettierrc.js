module.exports = {
  useTabs: false,
  semi: true,
  trailingComma: 'es5',
  singleQuote: true,
  printWidth: 80,
  tabWidth: 2,
  jsxBracketSameLine: false,
  parser: 'flow',
};
